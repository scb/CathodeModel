# MatLab Files Associated with Cathode Model Paper

## Index of MatLab Files in Root

### inputs2B.m

#### Purpose: This function file contains parameters for the model

#### Syntax: inputs2b(Air,naf,ptot,masscat,density,Tcell,Tdew)

Air: cathode gas inlet composition: true - air, false-oxygen
default: true

naf: mass fraction of ionomer
default: 0.45

ptot: absolute pressure of gases in atmospheres
default: 3 atm

masscat: catalyst loading in mg of catalyst per square cm
default: 4 mg/cm2

density: density of catalyst layer in g catalyst per cubic cm
default: 0.32 gcat/cm3

Tcell: cell temperature in K
default: 353 K

Tdew: dew point of inlet gas in K
default: 353 K (100 % RH)

### PCfitfunVarAirParCompfHOkw.m

#### Purpose: This function fits parameters: i0, U, alpha, fHO, and kw

#### Syntax:PCfitfunVarAirParCompfHOkw(in,dataf,datag,pvals,mvals,parcomp)

in: input structure containing guesses for the parameters

dataf: data to be fit. it is a structure containing two matrices: ix and vx
ix contains current values with rows for each potential and columns for each
pressure. There is a structure element for each loading to be considered

datag: initial guesses for each value in dataf. Each element of datag corresponds to each loading in dataf. Each data point contains a structure "v" where each element of v contains an output from the model

pvals: total pressure for data to be considered (in atmospheres)

mvals: catalyst loadings for data to be considered (in mg catalyst /cm2)

parcomp: binary variable expressing whether parallel computing will be used

### PEMFC_NDBVKn.m			

#### Purpose: Model function solving nine differential equations over two regions considering the gas, liquid, ionic, and electronic transport in the catalyst layer and gas diffusion layer of a PEMFC cathode. Uses Butler-Volmer (BV) kinetics and considers (Knudsen Diffusion)

#### Syntax: PEMFC_NDBVKn(input,sol)

input: input structure containing model parameters

sol: guessed solution 

### scriptG2BParCompFit.m

#### purpose: script file that runs PCfitfunVarAirParCompfHOkw.m

### scriptPaperCalcs.m

#### purpose: scriptfile that runs PEMFC_NDBVKn.m at a variety of conditions in order to generation the numbers necessary for the figures in the paper


