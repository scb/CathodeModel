function out=PEMFC_NDBV(input,sol)
%Model of PEMFC Cathode including membrane


%% Check if corrent inputs are available, if not choose default

if exist('input','var')
    in=input;
else
    in=inputs2B;
    in.fig=1;
end
out.in=in;

if exist('sol','var')
    guesssoln=sol.NDsol;
else
    load('ExtraFiles/GuessValsND_CL_GDL.mat','out')
    guesssoln=out.NDsol;
end



% reformat a solution file into a guess file:
% remove duplicate points at region boundaries
xvals=[];
yvals=[];
for xx=1:length(guesssoln.x)-1
    if guesssoln.x(xx)~=guesssoln.x(xx+1)
        xvals=[xvals;guesssoln.x(xx)];
        yvals=[yvals;guesssoln.y(xx,:)];
    end %if
end %for

%% Non-dimensional groups

% Catalyst Layer (Region 1)
innd=in.CL;
ND.rho=innd.rho;
ND.sr=innd.sr;
ND.ro=innd.ro;
ND.fH=innd.fH;
ND.fk=innd.fk;
ND.fHt=sum(innd.fH.*innd.fk);
ND.PI.i=in.n*in.F*in.Don./(in.B*innd.kieff*in.R*in.T).*0.1; %0.1 Pa*m3/bar*cc
ND.PI.e=in.n*in.F*in.Don./(in.B*innd.keeff*in.R*in.T).*0.1; %0.1 Pa*m3/bar*cc
ND.PI.L=in.Don*in.mu*in.V0/(innd.kw*in.R*in.T*in.ptot).*0.1; %0.1 Pa*m3/bar*cc
ND.PI.on=1./(innd.rho.^(3/2)*ND.fHt.^(3/2));
ND.PI.ow=in.Don./(in.Dow*innd.rho.^(3/2)*ND.fHt.^(3/2));
ND.PI.wn=in.Don./(in.Dwn*innd.rho.^(3/2)*ND.fHt.^(3/2));
ND.Vkn=2./3.*(8.*in.R.*in.T./pi./in.MW).^0.5.*1e2;          %[=]m/s * 1e2cm/m = cm/s
ND.H=-2.*in.gamma.*cos(innd.theta)./(in.ptot.*innd.ro).*10;  %10 um*bar/m*Pa
ND.Thevap=in.kmat*in.R*in.T*innd.L*in.ptot./in.Don.*10; %10 bar*cc/Pa*m3
ND.Th2=in.ioeff*in.R*in.T*innd.L.^2*in.ptot.*innd.vfn.*innd.rho.*innd.vfs./(in.n*in.F*in.Don).*10; %10 bar*cc/Pa*m3
out.CL=ND;

% Gas Diffusion Layer (Region 2)
innd=in.GDL;
ND.rho=innd.rho;
ND.sr=innd.sr;
ND.ro=innd.ro;
ND.fH=innd.fH;
ND.fk=innd.fk;
ND.fHt=sum(innd.fH.*innd.fk);
ND.PI.e=in.n*in.F*in.Don./(in.B*innd.keeff*in.R*in.T).*0.1; %0.1 Pa*m3/bar*cc
ND.PI.L=in.Don*in.mu*in.V0/(innd.kw*in.R*in.T*in.ptot).*0.1; %0.1 Pa*m3/bar*cc
ND.PI.on=1./(innd.rho.^(3/2)*ND.fHt.^(3/2));
ND.PI.ow=in.Don./(in.Dow*innd.rho.^(3/2)*ND.fHt.^(3/2));
ND.PI.wn=in.Don./(in.Dwn*innd.rho.^(3/2)*ND.fHt.^(3/2));
ND.Vkn=2./3.*(8.*in.R.*in.T./pi./in.MW).^0.5.*1e2;
ND.H=-2.*in.gamma.*cos(innd.theta)./(in.ptot.*innd.ro).*10;  %10 um*bar/m*Pa
ND.Thevap=in.kmat*in.R*in.T*innd.L*in.ptot./in.Don.*10; %10 bar*cc/Pa*m3
out.GDL=ND;

% General
out.Ps0=(in.U-in.pot)./in.B;
out.PI.mem=in.n*in.F*in.Don*in.Mem.L./(in.B*in.Mem.km*in.R*in.T*in.CL.L).*0.1; %0.1 Pa*m3/bar*cc
out.PI.Rext=in.n*in.F*in.Don*in.Rext./(in.B*in.R*in.T*in.GDL.L).*0.1; %0.1 Pa*m3/bar*cc
%% Initial conditions

N=10;                          % Number of divisions

z1=logspace(-2,0,N);z2=1-logspace(-2,0,N);zi=sort([z1 z2]);

z=[zi,zi+1];
% Catalyst layer thickness Vector (unitless)
% combination of logspacing from both sides because regions near interfaces
% will probably have steeper gradients. Program automatically adds points
% if there are not enough to solve with resolution required

%% Initialize BVP problem

solinit=bvpinit(z,@guess);

%% Solve Problem

options=bvpset('NMax',100000,'RelTol',1e-4,'AbsTol',1e-7);%,'Stats','on');
%default values:'NMax',floor(1000/n),'RelTol',1e-3,'AbsTol',1e-6

sol=bvp5c(@deq,@bc,solinit,options);

%% Calculate derived parameters

xf=length(sol.x);
sol.y=sol.y';
sol.x=sol.x';
pcapv=sol.y(:,3)-in.ptot;
for zz=1:length(sol.x)
    if sol.x(zz)<=1
        NDcase=out.CL;
    else
        NDcase=out.GDL;
    end % if region
    sol.y(zz,10)=1-sol.y(zz,8)-sol.y(zz,9);
    rsi=RadiusCrit(sol.y(zz,6),NDcase.H);
    Si(zz)=satfun(rsi,NDcase.rho,NDcase.fH,NDcase.sr,NDcase.fk);
    sol.y(zz,11)=Si(zz);
    sol.y(zz,12)=(NDcase.fHt./(1-Si(zz))).^(3/2);
    
end %for zz

if in.fig
    iLdep=[1 3 5 7];
    textstr(1).title='electronic current';
    textstr(2).title='electronic over-potential';
    textstr(3).title='ionic current';
    textstr(4).title='ionic over-potential';
    textstr(5).title='liquid flux';
    textstr(6).title='liquid pressure';
    textstr(7).title='water vapor flux';
    textstr(8).title='oxygen vapor fraction';
    textstr(9).title='water vapor fraction';
    textstr(10).title='nitrogen vapor fraction';
    textstr(11).title='Saturation';
    textstr(12).title='Capillary Effects';
    figure(2)
    subplot(4,1,1)
    fluxes=[1 3 5 7];
    plot(sol.x,sol.y(:,fluxes))
    legend(textstr(1).title,textstr(3).title,textstr(5).title,textstr(7).title)
    %ylim([1e-10 1])
    subplot(4,1,2)
    pots=[2 4];
    plot(sol.x,sol.y(:,pots))
    legend(textstr(2).title,textstr(4).title)
    subplot(4,1,3)
    fracs=[8 9 10];
    plot(sol.x,sol.y(:,fracs))
    legend(textstr(8).title,textstr(9).title,textstr(10).title)
    subplot(4,1,4)
    othvals=[6 11 12];
    plot(sol.x,sol.y(:,othvals))
    legend(textstr(6).title,textstr(11).title,textstr(12).title)
end %figure
sol.I=sol.y(1,3);
out.NDsol=sol;
out.I=sol.I*in.Don*in.n*in.F/in.R/in.T./in.CL.L.*0.1; %0.1 Pa*m3/bar*cc
%% Auxilliary Functions

%% Governing Equations (function #1)
    function dydz=deq(z,y,region)
        
        Qe=y(1);    % Electronic Current
        Pse=y(2);   % Electronic Potential
        Qi=y(3);    % Ionic Current
        Psi=y(4);   % Ionic Potential
        QL=y(5);    % Liquid Flux
        PL=y(6);    % Liquid Pressure
        Qw=y(7);    % Water Vapor Flux
        xo=y(8);    % Oxygen Vapor Fraction
        xw=y(9);    % Water Vapor Fraction
        
        
        % partial pressure of water vapor (three constituent gas phase)
        xn=1-xo-xw;
        
        % conservation of charge removes one differential equation
        Qo=-Qe;
        
        switch region
            case 1    % x in [0 1] Catalyst Layer
                NDcase=out.CL;
                % saturation (see function below)
                rsCL=RadiusCrit(PL,NDcase.H);
                [S Se]=satfun(rsCL,NDcase.rho,NDcase.fH,NDcase.sr,NDcase.fk);
                
                % Relative Liquid Permeability
                kwr=relperm(rsCL,NDcase.fH,NDcase.sr,Se,NDcase.fk);
                
                % Knudsen Diffusion Factor
                [Fkn Dkn rkn] =Knudsen(rsCL,NDcase.fH,NDcase.sr,NDcase.ro,NDcase.fk,NDcase.Vkn);
                
                % Generation Term: Evaporation
                jevap=NDcase.Thevap*(psatfun(PL)./in.ptot-xw);
                
                % Generation Term: Oxygen Reduction Reaction
                ored=(1-S).*NDcase.Th2.*(xo.*exp((in.alpha).*(out.Ps0-(Pse-Psi)))-exp(-(1-in.alpha).*(out.Ps0-(Pse-Psi)))./in.ptot);
                
                
                % Flux Mass Balances:
                dQwdz=jevap;           % Water Vapor
                dQLdz=(-jevap+ored*2);
                dQodz=-ored;
                dQidz=-ored;
                dQedz=ored;
                
                % Stefan Maxwell Binary Diffusion with air and water vapor
                Qn=0;       % conservation of mass: no nitrogen consumed so net flux is zero
                % pressure gradient is sum of various binary diffusion components
                dxwodz=(xw*(Qo)-(xo)*Qw)*NDcase.PI.ow.*(NDcase.fHt./(1-S)./Fkn(1)).^(3/2);
                dxwndz=(xw*(Qn)-(xn)*Qw)*NDcase.PI.wn.*(NDcase.fHt./(1-S)./Fkn(1)).^(3/2);
                dxwdz=dxwodz+dxwndz;
                
                dxondz=(xo*(Qn)-(xn)*Qo)*NDcase.PI.on.*(NDcase.fHt./(1-S)./Fkn(2)).^(3/2);
                dxowdz=(xo*(Qw)-(xw)*Qo)*NDcase.PI.ow.*(NDcase.fHt./(1-S)./Fkn(2)).^(3/2);
                dxodz=dxowdz+dxondz;
                
                dxnwdz=(xn*(Qw)-(xw)*Qn)*NDcase.PI.wn.*(NDcase.fHt./(1-S)./Fkn(3)).^(3/2);
                dxnodz=(xn*(Qo)-(xo)*Qn)*NDcase.PI.on.*(NDcase.fHt./(1-S)./Fkn(3)).^(3/2);
                dxndz=dxnodz+dxnwdz;
                
                % Pressure gradient in Liquid
                dPLdz=-QL*NDcase.PI.L./kwr;%
                
                % Ohmic losses
                dPsidz=-Qi*NDcase.PI.i;
                dPsedz=-Qe*NDcase.PI.e;
            case 2    % x in [1 2] Gas Diffusion Layer
                % saturation (see function bellow)
                NDcase=out.GDL;
                % saturation (see function bellow)
                rsGDL=RadiusCrit(PL,NDcase.H);
                [S Se]=satfun(rsGDL,NDcase.rho,NDcase.fH,NDcase.sr,NDcase.fk);
                
                % Relative Liquid Permeability
                kwr=relperm(rsGDL,NDcase.fH,NDcase.sr,Se,NDcase.fk);
                                
                % Knudsen Diffusion Factor
                [Fkn Dkn rkn] =Knudsen(rsGDL,NDcase.fH,NDcase.sr,NDcase.ro,NDcase.fk,NDcase.Vkn);
                
                % Generation Term: Evaporation
                jevap=NDcase.Thevap*(psatfun(PL)./in.ptot-xw);
                
                % Flux Mass Balances:
                dQwdz=jevap;           % Water Vapor
                dQLdz=-jevap;
                dQodz=0;
                dQidz=0;
                dQedz=0;
                
                % Stefan Maxwell Binary Diffusion with air and water vapor
                Qn=0;       % conservation of mass: no nitrogen consumed so net flux is zero
                % pressure gradient is sum of various binary diffusion components
                dxwodz=(xw*(Qo)-(xo)*Qw)*NDcase.PI.ow.*(NDcase.fHt./(1-S)./Fkn(1)).^(3/2);
                dxwndz=(xw*(Qn)-(xn)*Qw)*NDcase.PI.wn.*(NDcase.fHt./(1-S)./Fkn(1)).^(3/2);
                dxwdz=dxwodz+dxwndz;
                
                dxondz=(xo*(Qn)-(xn)*Qo)*NDcase.PI.on.*(NDcase.fHt./(1-S)./Fkn(2)).^(3/2);
                dxowdz=(xo*(Qw)-(xw)*Qo)*NDcase.PI.ow.*(NDcase.fHt./(1-S)./Fkn(2)).^(3/2);
                dxodz=dxowdz+dxondz;
                
                dxnwdz=(xn*(Qw)-(xw)*Qn)*NDcase.PI.wn.*(NDcase.fHt./(1-S)./Fkn(3)).^(3/2);
                dxnodz=(xn*(Qo)-(xo)*Qn)*NDcase.PI.on.*(NDcase.fHt./(1-S)./Fkn(3)).^(3/2);
                dxndz=dxnodz+dxnwdz;
                
                % Pressure gradient in Liquid
                dPLdz=-QL*NDcase.PI.L./kwr;%
                
                % Ohmic losses
                dPsidz=0;
                dPsedz=-Qe*NDcase.PI.e;
            otherwise
                error('MATLAB:threebvp:BadRegionIndex','Incorrect region index: %d',region);
        end
        
        
        dydz=[dQedz;dPsedz;dQidz;dPsidz;...
            dQLdz;dPLdz;dQwdz;dxodz;dxwdz];%
        %figure(1)
        %plot(z,y)
        %hold on
        
    end

%% Boundary Condition Function  (function #2)

    function res=bc(yL,yR)
        %  Membrane (L) CL (R) (L) GDL (R) Channel
        QeM=yL(1,1);    QeC=yR(1,2);    % Electronic Current
        PseM=yL(2,1);   PseC=yR(2,2);   % Electronic Over-potential
        QiM=yL(3,1);    QiC=yR(3,2);    % Ionic Current
        PsiM=yL(4,1);   PsiC=yR(4,2);   % Ionic Over-potential
        QLM=yL(5,1);    QLC=yR(5,2);    % Liquid Flux
        PLM=yL(6,1);    PLC=yR(6,2);    % Liquid Pressure
        QwM=yL(7,1);    QwC=yR(7,2);    % Water Vapor Flux
        xoM=yL(8,1);    xoC=yR(8,2);    % Oxygen Vapor Fraction
        xwM=yL(9,1);    xwC=yR(9,2);    % Water Vapor Fraction
        
        % Potentials and Currents:
        res(1)=QeM;
        % electric current is zero at membrane
        res(2)=PseC-QeC*out.PI.Rext;
        % electric conductivity overpotential is external ohmic losses
        % @channel
        res(3)=QiC;
        % ionic current is zero at GDL
        res(4)=PsiM+QiM*out.PI.mem;
        % ionic conductivity overpotential is membrane ohmic losses at Membrane
        
        % Liquid Phase:
        res(5)=in.Mem.beta*QiM-QLM;
        % mass balance at Membrane-Cathode interface: water flux out of
        % membrane = water flux into cathode at L=0;
        res(6)=PLC-1;%
        % liquid pressure = gas pressure at GDL cathode interface (no
        % capillary pressure)
        
        % Vapor Phase:
        res(7)=QwM;%
        % membrane is gas impermeable
        res(8)=xoC-in.xo;
        % oxygen vapor fraction is bulk conditions at Channel
        res(9)=xwC-in.xw;
        % vapor fraction is bulk conditions at Channel
        
        iLdep=[1 3 5 7];
        resLdep=yR(iLdep,1)./in.CL.L-yL(iLdep,2)./in.GDL.L;
        
        inorm=[2 4 6 8 9];
        resnorm=yR(inorm,1)-yL(inorm,2);
        
        res=[res(:);resLdep(:);resnorm(:)];
    end
%% Guess Function  (function #3)
% provides initial guesses for bvp

    function yg=guess(xg,region)
       
        yg=interp1(xvals,yvals(:,1:9),xg,'linear','extrap');
        
        yg=yg(:);
    end


%% Saturation  (function #4)

% calculation of saturation as a function of critical radius, rc (see
% function 6); porosity, eo; hydrophobic pore fraction, fH; and pore size
% distibution, ro and sk (characteristic radius and spread of a
% log-normgal distribution)

% in this version we are simplifying the calcs by assuming capillary
% pressure is always greater than zero. That means we don't have to do any
% calcs for hydrophilic pores because they are always filled

    function [sval svale] =satfun(rsi,eo,fH,sk,fk)
        
        
        % Residual wetting saturation (Eqn 8, Ref.1)
        sat0L=-5.3202*eo^5+17.062*eo^4-21.706*eo^3+13.692*eo^2-4.816*eo+0.9989;
        
        if sat0L <= 0.15
            sat0G = 1 - sat0L;
        else
            sat0G = 0.85;
        end %if
        
        for nn=1:length(fk)
            if rsi(nn) > 0
                sH(nn)=(1-erf((log(rsi(nn)))./(sk(nn)*sqrt(2))))./2;
                
            else
                sH(nn)=0;
            end %if
            
            svalk(nn)=fH(nn).*sH(nn)+(1-fH(nn));
        end %for nn
        sval=sum(svalk.*fk);
        % Effective saturation (Eqn 17, Ref.1)
        svale=(sval-sat0L)./(1-sat0L);
        if svale<0
            svale=0;
            error('impossible saturation value')
        end %if
        
    end %function

%% Permeability  (function #5)

% calculation of permiabilty as a function of critical radius, rc (see
% function 6); porosity, eo; ; hydrophobic pore fraction, fH; pore size
% distibution, ro and sk (characteristic radius and spread of a
% log-normgal distribution); and effective saturation, svale. (Ref.1)

    function kvali =relperm(rsi,fH,sk,svale,fk)
        
        for nn=1:length(fk)
        if rsi(nn) > 0
            krHO(nn)=(1-...
                erf(((log(rsi(nn)))./(sk(nn)*sqrt(2)))-(sk(nn)*sqrt(2))));
        else
            krHO(nn)=0;
        end %if
        
        if krHO(nn) <0
            krHO(nn)=0;
            error('impossible permeability value')
        end %if
        end % for each PSD
        krHI=1;
        
        kvali=sum((svale.^2./2.*fH.*krHO+(1-fH).*krHI).*fk);
        
        
        
    end %function


%% Critical Radius  (function #6)

% (Ref.1)

    function rsi=RadiusCrit(PLi,Hho)
        rsi=Hho./(PLi-1);
    end % function psat


%% Saturation Pressure  (function #7)

% saturation pressure as a function of capillary pressure: as capillary
% pressure rises, so does saturation pressure (Ref.1)

    function psatf=psatfun(pL)
        psatf=in.p0vap*exp((pL-1)*in.ptot*in.V0/in.R/in.T);
    end % function psat



%% Knudsen Factors  (function #8)

% calculation of Knudsen Diffusion as a function of critical radius, rsi (see
% function 6); porosity, eo; ; hydrophobic pore fraction, fH; pore size
% distibution, ro and sk (characteristic radius and spread of a
% log-normgal distribution), and molecular weights of gases, Vvals; (Ref.1)

    function [Fkn Dkn rkn] =Knudsen(rsi,fH,sk,rk,fk,Vvals)
        
        for nn=1:length(fk)
        if rsi(nn) > 0
            rknHO(nn)=rk(nn).*exp(sk(nn).^2./2).*...
                (1+erf(((log(rsi(nn)))./(sk(nn)*sqrt(2)))-(sk(nn)/sqrt(2))))./...
                (1+erf(((log(rsi(nn)))./(sk(nn)*sqrt(2)))));
        else
            rknHO(nn)=rk(nn);
        end %if
        
        if rknHO(nn) <0
            rknHO(nn)=0;
            error('impossible permeability value')
        end %if
        end % for each PSD
        rkn=sum(fk.*rknHO);
        
        Dkn=rkn.*Vvals.*1e-4;
        
        Fkn=Dkn./(in.Don+Dkn);
        
        
    end %function

end
% References:
%1.   Weber, A. Z., Darling, R. M., & Newman, J. (2004). Modeling Two-Phase
%   Behavior in PEFCs. Journal of The Electrochemical Society, 151(10), A1715.
%   doi:10.1149/1.1792891
%2.   R. B. Bird, W. E. Stewart, and E. N. Lightfoot, Transport Phenomena,
%   John Wiley
%3.   Weber, A. Z., & Newman, J. (2004). Transport in Polymer-Electrolyte
%   Membranes. Journal of The Electrochemical Society, 151(2), A311.
%   doi:10.1149/1.1639157


