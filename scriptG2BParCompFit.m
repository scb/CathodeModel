
%% Comparison of Model with Experimental Results
parcomp=1;
% if parcomp is zero program runs without parallel computing
if parcomp==1% if parcomp is one it runs with parallel computing
    matlabpool('open',20)
else
    tic
    figure(1)   % in fitting function
    figure(2)   % in fitting function
    figure(3)   % plot of initial model fit
end %if

% Model Results for Figures in Paper (Figs. 5-8)
load('MEAdata/Gen2BLot0813varload.mat')
%load('PolarizationGuessFit.mat')
%tic

data(1).V=data2.V;
data(2).V=data3.V;
data(3).V=data4.V;
data(1).I=data2.I;
data(2).I=data3.I;
data(3).I=data4.I;
mvals=2:4; % we have two loadings
vvs=1:33; % we have 33 voltage points
vvmat=[vvs(:),vvs(:),vvs(:)];
% Extent of data we want to use because I am uncertain of currents close to
% zero. There seems to be a background current that is pressure and loading
% dependent so we will cut the data off at 2x and 10x the background current
 for mm=1:3
        datac(mm).I=abs(ones(33,1)*min(data(mm).I,[],1));
        
       datac(mm).goodvv1=(data(mm).I >= 2.*datac(mm).I).*vvmat;
        vvmax1(mm,:)=max(datac(mm).goodvv1,[],1);
        vvmin1(mm,:)=34-vvmax1(mm,:);
         datac(mm).goodvv2=(data(mm).I >= 10.*datac(mm).I).*vvmat;
        vvmax2(mm,:)=max(datac(mm).goodvv2,[],1);
        vvmin2(mm,:)=34-vvmax2(mm,:);
end %for


psig=[2 12 30]; % psi_g (gauge pressure)
pvals=psig./14.7+0.83; %Calculate absolute pressure in atmospheres


in=inputs2B;
outnd0=PEMFC_NDBVKnf(in);
mmfit=[1:3];
vvfit=[1:33];
ppfit=[1:3];

%% The model is run with guessed values.
% These values become the guess for the fitting function
for mm=1:length(mmfit)
    
    for pp=1:length(ppfit)
        
        outnd=outnd0;
        vvals=data(mm).V(vvfit,pp);
        
        for vv=1:length(vvfit)
            dataf(mm).ix(vv,pp)=data(mm).I(vvfit(vv),ppfit(pp));
            dataf(mm).vx(vv,pp)=data(mm).V(vvfit(vv),ppfit(pp));
            in=inputs2B(1,0.45,pvals(ppfit(pp)),mvals(mm));
            in.pot=vvals(vv);
            po(pp)=in.xo.*in.ptot;
            outnd=PEMFC_NDBVKnf(in,outnd);
            if parcomp==1
            else
                toc
                tic
            end %if
            datag(mm).v(vv,pp).solair=outnd;
            imod(mm).v(vv,pp)=datag(mm).v(vv,pp).solair.I;
            vmod(mm).v(vv,pp)=vvals(vv);
        end %for vv
        
    end %for pp
end %for mm
%% Graph Data in Single Node Mode
if parcomp==0 % if we are not using parallel computing we will be
    %interested in seeing what the data looks like
    figure(3)
    semilogx(dataf(1).ix,dataf(1).vx,'o',dataf(2).ix,dataf(2).vx,'o',...
        dataf(mm).ix,dataf(mm).vx,'o')
    hold on
    plot(imod(1).v,vmod(1).v,imod(2).v,vmod(2).v,...
        imod(mm).v,vmod(mm).v)
    hold off
    xlim([0 2])
    xlabel('Current Density  / A cm^-^2','FontSize',12)
    ylabel('Potential / V','FontSize',12)
    title('Comparison with Experimental Results','FontSize',12)
    legend('1.0','1.7','2.9','1.0','1.7','2.9',...
        '1.0','1.7','2.9')
    
end %if parcomp==0

%% Fitting hydrophobic pore fraction, and permeability*
% *also the three kinetic parameters i0,U,alpha
% first we have to choose what data we want to fit
in=inputs2B;
ppfit2=[1:3];pvalsf2=pvals(ppfit2); % what pressures we want to fit
mmfit2=[1:3];mvalsf2=mvals(mmfit2); % what loadings we want to fit
vvmin=vvmin1;
vvmax=vvmax1;
vvnum=min(min(vvmax(mmfit2,ppfit2)-vvmin(mmfit2,ppfit2)));


for mm=1:length(mmfit2) %Now we select the data we have chosen
    for pp=1:length(ppfit2)
        vvfin=vvmax(mmfit2(mm),ppfit2(pp));
        vvfit2=vvfin-vvnum:vvfin; %what potentials
        % we want to fit
        for vv=1:length(vvfit2)
            datag2(mm).v(vv,pp).solair=datag(mmfit2(mm)).v(vvfit2(vv),ppfit2(pp)).solair;
            dataf2(mm).ix(vv,pp)=dataf(mmfit2(mm)).ix(vvfit2(vv),ppfit2(pp));
            dataf2(mm).vx(vv,pp)=dataf(mmfit2(mm)).vx(vvfit2(vv),ppfit2(pp));
        end %for vv
    end %for pp
end %for mm

% run fit function:
out= PCfitfunVarAirParCompfHOkw(in,dataf2,datag2,pvalsf2,mvalsf2,parcomp)

% Calculate bounds (but these are normalized)
cirel = nlparci(out.kvect,out.residual,...
    'jacobian',out.jacobian,'alpha',0.32);
% Calculate bounds (unnormalized)
ci=cirel.*(out.x0gf(:)*ones(1,2));
% Calculate standard deviation
outstd=abs(ci-out.xg(:)*ones(1,2))

% incorporate these values into the output structure
out.cirel=cirel;
out.ci=ci;
out.std=outstd;

% save output structure to a file that is date stamped
save(['outputfiles/', datestr(clock),'fHOkwoutput3.mat'],'out')

%% Fitting hydrophobic pore fraction, and permeability*
% *also the three kinetic parameters i0,U,alpha
% first we have to choose what data we want to fit
in=inputs2B;
ppfit2=[1:3];pvalsf2=pvals(ppfit2); % what pressures we want to fit
mmfit2=[1:3];mvalsf2=mvals(mmfit2); % what loadings we want to fit
vvmin=vvmin2;
vvmax=vvmax2;
vvnum=min(min(vvmax(mmfit2,ppfit2)-vvmin(mmfit2,ppfit2)));


for mm=1:length(mmfit2) %Now we select the data we have chosen
    for pp=1:length(ppfit2)
        vvfin=vvmax(mmfit2(mm),ppfit2(pp));
        vvfit2=vvfin-vvnum:vvfin; %what potentials
        % we want to fit
        for vv=1:length(vvfit2)
            datag2(mm).v(vv,pp).solair=datag(mmfit2(mm)).v(vvfit2(vv),ppfit2(pp)).solair;
            dataf2(mm).ix(vv,pp)=dataf(mmfit2(mm)).ix(vvfit2(vv),ppfit2(pp));
            dataf2(mm).vx(vv,pp)=dataf(mmfit2(mm)).vx(vvfit2(vv),ppfit2(pp));
        end %for vv
    end %for pp
end %for mm

% run fit function:
out= PCfitfunVarAirParCompfHOkw(in,dataf2,datag2,pvalsf2,mvalsf2,parcomp)

% Calculate bounds (but these are normalized)
cirel = nlparci(out.kvect,out.residual,...
    'jacobian',out.jacobian,'alpha',0.32);
% Calculate bounds (unnormalized)
ci=cirel.*(out.x0gf(:)*ones(1,2));
% Calculate standard deviation
outstd=abs(ci-out.xg(:)*ones(1,2))

% incorporate these values into the output structure
out.cirel=cirel;
out.ci=ci;
out.std=outstd;

% save output structure to a file that is date stamped
save(['outputfiles/', datestr(clock),'fHOkwoutput3.mat'],'out')


%% Fitting hydrophobic pore fraction, permeability, and ionic conductivity*
% *also the three kinetic parameters i0,U,alpha
% first we have to choose what data we want to fit
in=inputs2B;
ppfit2=[1:3];pvalsf2=pvals(ppfit2); % what pressures we want to fit
mmfit2=[1:3];mvalsf2=mvals(mmfit2); % what loadings we want to fit
vvnum=min(min(vvmax(mmfit2,ppfit2)-vvmin(mmfit2,ppfit2)));


for mm=1:length(mmfit2) %Now we select the data we have chosen
    for pp=1:length(ppfit2)
        vvfin=vvmax(mmfit2(mm),ppfit2(pp));
        vvfit2=vvfin-vvnum:vvfin; %what potentials
        % we want to fit
        for vv=1:length(vvfit2)
            datag2(mm).v(vv,pp).solair=datag(mmfit2(mm)).v(vvfit2(vv),ppfit2(pp)).solair;
            dataf2(mm).ix(vv,pp)=dataf(mmfit2(mm)).ix(vvfit2(vv),ppfit2(pp));
            dataf2(mm).vx(vv,pp)=dataf(mmfit2(mm)).vx(vvfit2(vv),ppfit2(pp));
        end %for vv
    end %for pp
end %for mm

% run fit function:
out= PCfitfunVarAirParCompfHOkwki(in,dataf2,datag2,pvalsf2,mvalsf2,parcomp)

% Calculate bounds (but these are normalized)
cirel = nlparci(out.kvect,out.residual,...
    'jacobian',out.jacobian,'alpha',0.32);
% Calculate bounds (unnormalized)
ci=cirel.*(out.x0gf(:)*ones(1,2));
% Calculate standard deviation
outstd=abs(ci-out.xg(:)*ones(1,2))

% incorporate these values into the output structure
out.cirel=cirel;
out.ci=ci;
out.std=outstd;

% save output structure to a file that is date stamped
save(['outputfiles/', datestr(clock),'fHOkwkioutput3.mat'],'out')


%% Close the node cluster if we are using parallel computing
if parcomp==1
    matlabpool('close')
end %if